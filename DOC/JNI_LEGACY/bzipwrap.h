/*

    Copyright 2013, 2014, 2015, 2016, 2017, 2018, 2019  joshua.tee@gmail.com

    This file is part of wX.

    wX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    wX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with wX.  If not, see <http://www.gnu.org/licenses/>.

 */

#include <jni.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>


#ifndef _Included_joshuatee_wX_JNI_bzipwrapfull
#define _Included_joshuatee_wX_JNI_bzipwrapfull
#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jint JNICALL Java_joshuatee_wx_JNI_bzipwrapfull
  (JNIEnv * , jclass, jstring, jstring ,jobject,jobject, jint);

#ifdef __cplusplus
}
#endif
#endif

#ifndef _Included_joshuatee_wX_UtilityNexradL2OGL_bzipwrap
#define _Included_joshuatee_wX_UtilityNexradL2OGL_bzipwrap
#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jint JNICALL Java_joshuatee_wx_JNI_bzipwrap
  (JNIEnv * , jclass, jobject , jint, jobject, jint  );

#ifdef __cplusplus
}
#endif
#endif
