/*

    Copyright 2013, 2014, 2015, 2016, 2017, 2018, 2019  joshua.tee@gmail.com

    This file is part of wX.

    wX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    wX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with wX.  If not, see <http://www.gnu.org/licenses/>.

*/
//modded by ELY M. 


package joshuatee.wx.radar

import java.nio.ByteBuffer
import java.nio.ByteOrder
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.RectF
import android.opengl.GLSurfaceView.Renderer
import android.opengl.GLES20
import android.opengl.Matrix
import android.os.Environment

import joshuatee.wx.JNI
import joshuatee.wx.MyApplication
import joshuatee.wx.objects.GeographyType
import joshuatee.wx.objects.PolygonType
import joshuatee.wx.objects.ProjectionType
import joshuatee.wx.radarcolorpalettes.ObjectColorPalette
import joshuatee.wx.settings.UtilityLocation
import joshuatee.wx.util.*
import joshuatee.wx.objects.DistanceUnit
import java.lang.Math.*
import java.nio.FloatBuffer
import java.nio.ShortBuffer


class WXGLRender(private val context: Context) : Renderer {

    // The is the OpenGL rendering engine that is used on the main screen and the main radar interface
    // The goal is to be highly performant and configurable as such this module relies on C code accessed via JNI extensively
    // Java can also be used in set in settings->radar

    companion object {
        var ridGlobal: String = ""
            private set
        var positionXGlobal: Float = 0f
            private set
        var positionYGlobal: Float = 0f
            private set
        const val ortIntGlobal: Int = 400
        var oneDegreeScaleFactorGlobal: Float = 0.0f
            private set

        var degreesPerPixellat = 0.017971305190311 //had -
        var degreesPerPixellon = 0.017971305190311
        var north: Double = 0.toDouble()
        var south: Double = 0.toDouble()
        var west: Double = 0.toDouble()
        var east: Double = 0.toDouble()


        var newbottom: Double = 0.toDouble()
        var newleft: Double = 0.toDouble()

    }

    // this string is normally no string but for dual pane will be set to either 1 or 2 to differentiate timestamps
    var radarStatusStr: String = ""
    var idxStr: String = "0"
    private val mtrxProjection = FloatArray(16)
    private val mtrxView = FloatArray(16)
    private var mtrxProjectionAndView = FloatArray(16)
    var ridNewList: List<RID> = listOf()
    private var radarChunkCnt = 0
    private var lineCnt = 0
    private val breakSizeLine = 30000
    private val mtrxProjectionAndViewOrig = FloatArray(16)
    private var triangleIndexBuffer: ByteBuffer = ByteBuffer.allocate(0)
    private var lineIndexBuffer: ByteBuffer = ByteBuffer.allocate(0)
    private var gpsX = 0.toDouble()
    private var gpsY = 0.toDouble()
    private val zoomToHideMiscFeatures = 0.5f
    private val radarBuffers = ObjectOglRadarBuffers(context, MyApplication.nexradRadarBackgroundColor)
    private val spotterBuffers = ObjectOglBuffers(PolygonType.SPOTTER, zoomToHideMiscFeatures)
    private val stateLineBuffers = ObjectOglBuffers(GeographyType.STATE_LINES, 0.0f)
    private val countyLineBuffers = ObjectOglBuffers(GeographyType.COUNTY_LINES, 0.75f)
    private val hwBuffers = ObjectOglBuffers(GeographyType.HIGHWAYS, 0.45f)
    private val hwExtBuffers = ObjectOglBuffers(GeographyType.HIGHWAYS_EXTENDED, 3.00f)
    private val lakeBuffers = ObjectOglBuffers(GeographyType.LAKES, zoomToHideMiscFeatures)
    private val stiBuffers = ObjectOglBuffers(PolygonType.STI, zoomToHideMiscFeatures)
    private val wbBuffers = ObjectOglBuffers(PolygonType.WIND_BARB, zoomToHideMiscFeatures)
    private val wbGustsBuffers = ObjectOglBuffers(PolygonType.WIND_BARB_GUSTS, zoomToHideMiscFeatures)
    private val mpdBuffers = ObjectOglBuffers(PolygonType.MPD)
    private val hiBuffers = ObjectOglBuffers(PolygonType.HI, zoomToHideMiscFeatures)
    private val tvsBuffers = ObjectOglBuffers(PolygonType.TVS, zoomToHideMiscFeatures)
    private val warningFfwBuffers = ObjectOglBuffers(PolygonType.FFW)
    private val warningTstBuffers = ObjectOglBuffers(PolygonType.TST)
    private val warningTorBuffers = ObjectOglBuffers(PolygonType.TOR)
    private val watchBuffers = ObjectOglBuffers(PolygonType.WATCH)
    private val watchTornadoBuffers = ObjectOglBuffers(PolygonType.WATCH_TORNADO)
    private val mcdBuffers = ObjectOglBuffers(PolygonType.MCD)
    private val swoBuffers = ObjectOglBuffers()
    private val locdotBuffers = ObjectOglBuffers(PolygonType.LOCDOT)
    private val locCircleBuffers = ObjectOglBuffers()
    private val wbCircleBuffers = ObjectOglBuffers(PolygonType.WIND_BARB_CIRCLE, zoomToHideMiscFeatures)
    private val conusRadarBuffers = ObjectOglBuffers(PolygonType.CONUS)
    private val colorSwo = IntArray(5)
    private var breakSize15 = 15000
    private val breakSizeRadar = 15000
    private var mPositionHandle = 0
    private var colorHandle = 0
    private var mSizeHandle = 0
    private var tdwr = false
    private var chunkCount = 0
    private var totalBins = 0
    private var totalBinsOgl = 0
    var displayHold: Boolean = false

    private val conusProjection = FloatArray(16)
    private val conusView = FloatArray(16)
    private var conusProjectionAndView = FloatArray(16)


    private var iTexture: Int = 0
    private var conusradarId = -1
    var zoom: Float = 1.0f
        set(scale) {
            field = scale
            listOf(locdotBuffers, hiBuffers, spotterBuffers, tvsBuffers, wbCircleBuffers).forEach {
                if (it.isInitialized) {
                    it.lenInit = it.type.size
                    it.lenInit = scaleLength(it.lenInit)
                    it.draw(pn)
                }
            }
            if (locdotBuffers.isInitialized && MyApplication.locdotFollowsGps) {
                locCircleBuffers.lenInit = locdotBuffers.lenInit
                UtilityWXOGLPerf.genCircleLocdot(locCircleBuffers, pn, gpsX, gpsY)
            }

        }
    private var mSurfaceRatio = 0f
    var x: Float = 0f
        set(x) {
            field = x
            positionXGlobal = x
        }
    var y: Float = 0f
        set(y) {
            field = y
            positionYGlobal = y
        }
    var rid: String = ""
        set(rid) {
            field = rid
            ridGlobal = rid
        }
    private var prod = "N0Q"
    private val defaultLineWidth = 2.0f
    private var warnLineWidth = 2.0f
    private var watmcdLineWidth = 2.0f
    private var ridPrefixGlobal = ""
    private var bgColorFRed = 0.0f
    private var bgColorFGreen = 0.0f
    private var bgColorFBlue = 0.0f
    val ortInt: Int = 400
    private val provider = ProjectionType.WX_OGL
    // this controls if the projection is mercator (nexrad) or 4326 / rectangular
    // after you zoom out past a certain point you need to hide the nexrad, show the mosaic
    // and reconstruct all geometry and warning/watch lines using 4326 projection (set this variable to false to not use mercator transformation )
    // so far, only the base geometry ( state lines, county, etc ) respect this setting
    private var useMercatorProjection = true
    private val rdL2 = WXGLNexradLevel2()
    val radarL3Object: WXGLNexradLevel3 = WXGLNexradLevel3()
    val rdDownload: WXGLDownload = WXGLDownload()
    private var pn = ProjectionNumbers()
    var product: String
        get() = prod
        set(value) {
            prod = value
        }

    init {
        bgColorFRed = Color.red(MyApplication.nexradRadarBackgroundColor) / 255.0f
        bgColorFGreen = Color.green(MyApplication.nexradRadarBackgroundColor) / 255.0f
        bgColorFBlue = Color.blue(MyApplication.nexradRadarBackgroundColor) / 255.0f
        warnLineWidth = MyApplication.radarWarnLinesize.toFloat()
        watmcdLineWidth = MyApplication.radarWatmcdLinesize.toFloat()
        try {
            triangleIndexBuffer = ByteBuffer.allocateDirect(12 * breakSize15)
            lineIndexBuffer = ByteBuffer.allocateDirect(4 * breakSizeLine)
        } catch (e: Exception) {
            UtilityLog.HandleException(e)
        }
        triangleIndexBuffer.order(ByteOrder.nativeOrder())
        triangleIndexBuffer.position(0)
        lineIndexBuffer.order(ByteOrder.nativeOrder())
        lineIndexBuffer.position(0)
        if (!MyApplication.radarUseJni) {
            UtilityWXOGLPerf.genIndex(triangleIndexBuffer, breakSize15, breakSize15)
            UtilityWXOGLPerf.genIndexLine(lineIndexBuffer, breakSizeLine * 4, breakSizeLine * 2)
        } else {
            JNI.genIndex(triangleIndexBuffer, breakSize15, breakSize15)
            JNI.genIndexLine(lineIndexBuffer, breakSizeLine * 4, breakSizeLine * 2)
        }
    }

    fun initGEOM() {
        totalBins = 0
        if (prod == "TV0" || prod == "TZL") {
            tdwr = true
            val oldRid = this.rid
            if (this.rid == "") {
                this.rid = oldRid
                tdwr = false
                prod = "N0Q"
            }
        }
        pn = ProjectionNumbers(context, this.rid, provider)
        oneDegreeScaleFactorGlobal = pn.oneDegreeScaleFactorFloat
    }

    // final arg is whether or not to perform decompression
    fun constructPolygons(fileName: String, urlStr: String, performDecomp: Boolean) {
        radarBuffers.fn = fileName
        totalBins = 0
        // added to allow animations to skip a frame and continue
        if (product == "TV0" || product == "TZL") {
            tdwr = true
            val oldRid = this.rid
            if (this.rid == "") {
                this.rid = oldRid
                tdwr = false
                product = "N0Q"
            }
        }
        // if fn is empty string then we need to fetch the radar file
        // if set, its part of an anim sequence
        if (radarBuffers.fn == "") {
            ridPrefixGlobal = rdDownload.getRadarFile(context, urlStr, this.rid, prod, idxStr, tdwr)
            radarBuffers.fn = if (!product.contains("L2")) {
                val l3BaseFn = "nids"
                l3BaseFn + idxStr
            } else {
                "l2$idxStr"
            }
        }
        radarBuffers.setProductCodeFromString(product)
        try {
            when {
                product.contains("L2") -> {
                    rdL2.decocodeAndPlotNexradL2(
                            context,
                            radarBuffers.fn,
                            prod,
                            radarStatusStr,
                            idxStr,
                            performDecomp
                    )
                    radarBuffers.extractL2Data(rdL2)
                }
                product.contains("NSW") -> {
                    radarL3Object.decocodeAndPlotNexradLevel3FourBit(
                            context,
                            radarBuffers.fn,
                            radarStatusStr
                    )
                    radarBuffers.extractL3Data(radarL3Object)
                }
                product.contains("N0S") -> {
                    radarL3Object.decocodeAndPlotNexradLevel3FourBit(
                            context,
                            radarBuffers.fn,
                            radarStatusStr
                    )
                    radarBuffers.extractL3Data(radarL3Object)
                }
                product.contains("N1S") -> {
                    radarL3Object.decocodeAndPlotNexradLevel3FourBit(
                            context,
                            radarBuffers.fn,
                            radarStatusStr
                    )
                    radarBuffers.extractL3Data(radarL3Object)
                }
                product.contains("N2S") -> {
                    radarL3Object.decocodeAndPlotNexradLevel3FourBit(
                            context,
                            radarBuffers.fn,
                            radarStatusStr
                    )
                    radarBuffers.extractL3Data(radarL3Object)
                }
                product.contains("N3S") -> {
                    radarL3Object.decocodeAndPlotNexradLevel3FourBit(
                            context,
                            radarBuffers.fn,
                            radarStatusStr
                    )
                    radarBuffers.extractL3Data(radarL3Object)
                }
                else -> {
                    radarL3Object.decocodeAndPlotNexradDigital(
                            context,
                            radarBuffers.fn,
                            radarStatusStr
                    )
                    radarBuffers.extractL3Data(radarL3Object)
                }
            }
        } catch (e: Exception) {
            UtilityLog.HandleException(e)
        }
        if (radarBuffers.numRangeBins == 0) {
            radarBuffers.numRangeBins = 460
            radarBuffers.numberOfRadials = 360
        }
        radarBuffers.initialize()
        radarBuffers.setToPositionZero()
        val objColPal: ObjectColorPalette =
                if (MyApplication.colorMap.containsKey(radarBuffers.productCode.toInt())) {
                    MyApplication.colorMap[radarBuffers.productCode.toInt()]!!
                } else {
                    MyApplication.colorMap[94]!!
                }
        val cR = objColPal.redValues
        val cG = objColPal.greenValues
        val cB = objColPal.blueValues
        try {
            if (!product.contains("L2")) {
                totalBins =
                        if (radarBuffers.productCode != 56.toShort() && radarBuffers.productCode != 30.toShort()) {
                            if (!MyApplication.radarUseJni)
                                UtilityWXOGLPerf.decode8BitAndGenRadials(context, radarBuffers)
                            else {
                                JNI.decode8BitAndGenRadials(
                                        UtilityIO.getFilePath(context, radarBuffers.fn),
                                        radarL3Object.seekStart,
                                        radarL3Object.compressedFileSize,
                                        radarL3Object.iBuff,
                                        radarL3Object.oBuff,
                                        radarBuffers.floatBuffer,
                                        radarBuffers.colorBuffer,
                                        radarBuffers.binSize,
                                        Color.red(radarBuffers.bgColor).toByte(),
                                        Color.green(radarBuffers.bgColor).toByte(),
                                        Color.blue(radarBuffers.bgColor).toByte(),
                                        cR,
                                        cG,
                                        cB
                                )
                            }
                        } else {
                            UtilityWXOGLPerf.genRadials(
                                    radarBuffers,
                                    radarL3Object.binWord,
                                    radarL3Object.radialStart
                            )
                        }
            } else {
                rdL2.binWord.position(0)
                totalBins = if (MyApplication.radarUseJni)
                    JNI.level2GenRadials(
                            radarBuffers.floatBuffer,
                            radarBuffers.colorBuffer,
                            rdL2.binWord,
                            rdL2.radialStartAngle,
                            radarBuffers.numberOfRadials,
                            radarBuffers.numRangeBins,
                            radarBuffers.binSize,
                            radarBuffers.bgColor,
                            cR,
                            cG,
                            cB,
                            radarBuffers.productCode.toInt()
                    )
                else
                    UtilityWXOGLPerf.genRadials(radarBuffers, rdL2.binWord, rdL2.radialStartAngle)
            } // level 2 , level 3 check
        } catch (e: Exception) {
            UtilityLog.HandleException(e)
        }
        breakSize15 = 15000
        chunkCount = 1
        if (totalBins < breakSize15) {
            breakSize15 = totalBins
        } else {
            chunkCount = totalBins / breakSize15
            chunkCount += 1
        }
        radarBuffers.setToPositionZero()
        tdwr = false
        totalBinsOgl = totalBins
    }

    override fun onSurfaceCreated(gl: GL10, config: EGLConfig) {
        GLES20.glClearColor(bgColorFRed, bgColorFGreen, bgColorFBlue, 1f)
        OpenGLShader.sp_SolidColor = GLES20.glCreateProgram()
        GLES20.glAttachShader(
                OpenGLShader.sp_SolidColor,
                OpenGLShader.loadShader(GLES20.GL_VERTEX_SHADER, OpenGLShader.vs_SolidColor)
        )
        GLES20.glAttachShader(
                OpenGLShader.sp_SolidColor,
                OpenGLShader.loadShader(GLES20.GL_FRAGMENT_SHADER, OpenGLShader.fs_SolidColor)
        )
        GLES20.glLinkProgram(OpenGLShader.sp_SolidColor)
        GLES20.glUseProgram(OpenGLShader.sp_SolidColor)
        val vertexShaderUniform = OpenGLShaderUniform.loadShader(
                GLES20.GL_VERTEX_SHADER,
                OpenGLShaderUniform.vs_SolidColorUnfiform
        )
        val fragmentShaderUniform = OpenGLShaderUniform.loadShader(
                GLES20.GL_FRAGMENT_SHADER,
                OpenGLShaderUniform.fs_SolidColorUnfiform
        )
        OpenGLShaderUniform.sp_SolidColorUniform = GLES20.glCreateProgram()
        GLES20.glAttachShader(OpenGLShaderUniform.sp_SolidColorUniform, vertexShaderUniform)
        GLES20.glAttachShader(OpenGLShaderUniform.sp_SolidColorUniform, fragmentShaderUniform)
        GLES20.glLinkProgram(OpenGLShaderUniform.sp_SolidColorUniform)

        OpenGLShader.sp_loadimage = GLES20.glCreateProgram()
        GLES20.glAttachShader(OpenGLShader.sp_loadimage, OpenGLShader.loadShader(GLES20.GL_VERTEX_SHADER, OpenGLShader.vs_loadimage))
        GLES20.glAttachShader(OpenGLShader.sp_loadimage, OpenGLShader.loadShader(GLES20.GL_FRAGMENT_SHADER, OpenGLShader.fs_loadimage))
        GLES20.glLinkProgram(OpenGLShader.sp_loadimage)

        //shader for conus
        OpenGLShader.sp_loadconus = GLES20.glCreateProgram()
        GLES20.glAttachShader(OpenGLShader.sp_loadconus, OpenGLShader.loadShader(GLES20.GL_VERTEX_SHADER, OpenGLShader.vs_loadconus))
        GLES20.glAttachShader(OpenGLShader.sp_loadconus, OpenGLShader.loadShader(GLES20.GL_FRAGMENT_SHADER, OpenGLShader.fs_loadconus))
        GLES20.glLinkProgram(OpenGLShader.sp_loadconus)

        //test shader for conus
        OpenGLShader.sp_conus = GLES20.glCreateProgram()
        GLES20.glAttachShader(OpenGLShader.sp_conus, OpenGLShader.loadShader(GLES20.GL_VERTEX_SHADER, OpenGLShader.vs_conus))
        GLES20.glAttachShader(OpenGLShader.sp_conus, OpenGLShader.loadShader(GLES20.GL_FRAGMENT_SHADER, OpenGLShader.fs_conus))
        GLES20.glLinkProgram(OpenGLShader.sp_conus)

    }

    override fun onDrawFrame(gl: GL10) {
        GLES20.glUseProgram(OpenGLShader.sp_SolidColor)
        GLES20.glClearColor(bgColorFRed, bgColorFGreen, bgColorFBlue, 1f)
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)
        mPositionHandle = GLES20.glGetAttribLocation(OpenGLShader.sp_SolidColor, "vPosition")
        colorHandle = GLES20.glGetAttribLocation(OpenGLShader.sp_SolidColor, "a_Color")
        GLES20.glEnableVertexAttribArray(mPositionHandle)
        // required for color on VBO basis
        GLES20.glEnableVertexAttribArray(colorHandle)
        mtrxProjectionAndView = mtrxProjectionAndViewOrig
        Matrix.multiplyMM(mtrxProjectionAndView, 0, mtrxProjection, 0, mtrxView, 0)
        Matrix.translateM(mtrxProjectionAndView, 0, x, y, 0f)
        Matrix.scaleM(mtrxProjectionAndView, 0, zoom, zoom, 1f)
        GLES20.glUniformMatrix4fv(
                GLES20.glGetUniformLocation(
                        OpenGLShader.sp_SolidColor,
                        "uMVPMatrix"
                ), 1, false, mtrxProjectionAndView, 0
        )
        (0 until chunkCount).forEach {
            radarChunkCnt = if (it < chunkCount - 1) {
                breakSizeRadar * 6
            } else {
                6 * (totalBinsOgl - it * breakSizeRadar)
            }
            try {
                radarBuffers.floatBuffer.position(it * breakSizeRadar * 32)
                GLES20.glVertexAttribPointer(
                        mPositionHandle,
                        2,
                        GLES20.GL_FLOAT,
                        false,
                        0,
                        radarBuffers.floatBuffer.slice().asFloatBuffer()
                )
                radarBuffers.colorBuffer.position(it * breakSizeRadar * 12)
                GLES20.glVertexAttribPointer(
                        colorHandle,
                        3,
                        GLES20.GL_UNSIGNED_BYTE,
                        true,
                        0,
                        radarBuffers.colorBuffer.slice()
                )
                triangleIndexBuffer.position(0)
                GLES20.glDrawElements(
                        GLES20.GL_TRIANGLES,
                        radarChunkCnt,
                        GLES20.GL_UNSIGNED_SHORT,
                        triangleIndexBuffer.slice().asShortBuffer()
                )
            } catch (e: Exception) {
                UtilityLog.HandleException(e)
            }
        }
        GLES20.glLineWidth(defaultLineWidth)
        listOf(countyLineBuffers, stateLineBuffers, hwBuffers, hwExtBuffers, lakeBuffers).forEach {
            if (zoom > it.scaleCutOff) {
                drawElement(it)
            }
        }

        // FIXME
        // whether or not to respect the display being touched needs to be stored in
        // objectglbuffers. The wXL23 Metal code is more generic and thus each element drawn will need
        // to be checked. Will do this later when I have more time
        if (!displayHold) {
            listOf(spotterBuffers, hiBuffers, tvsBuffers).forEach {
                if (zoom > it.scaleCutOff) {
                    drawTriangles(it)
                }
            }
            GLES20.glLineWidth(3.0f)
            listOf(stiBuffers, wbGustsBuffers, wbBuffers).forEach {
                if (zoom > it.scaleCutOff) {
                    drawPolygons(it, 16)
                }
            }
            listOf(wbCircleBuffers).forEach {
                if (zoom > it.scaleCutOff) {
                    drawTriangles(it)
                }
            }
            //drawTriangles(wbCircleBuffers)
            GLES20.glLineWidth(defaultLineWidth)
            drawTriangles(locdotBuffers)
            if (MyApplication.locdotFollowsGps && locCircleBuffers.floatBuffer.capacity() != 0 && locCircleBuffers.indexBuffer.capacity() != 0 && locCircleBuffers.colorBuffer.capacity() != 0) {
                locCircleBuffers.chunkCount = 1
                drawPolygons(locCircleBuffers, 16)
            }
        }


        UtilityLog.d("wx", "zoom: " + zoom)
        if (MyApplication.radarConusRadar) {
            if (zoom < 0.093f) {
                UtilityLog.d("wx", "zoom out to conusradar")
                //useMercatorProjection = false
                //conusRadarBuffers.chunkCount = 1
                ///pn.radarSite = ""
                drawConusRadarTest(conusRadarBuffers)
            } else {
                //UtilityLog.d("wx", "back to mercator")
                //useMercatorProjection = true
            }
        }


        GLES20.glLineWidth(warnLineWidth)
        listOf(warningTstBuffers, warningFfwBuffers, warningTorBuffers).forEach { drawPolygons(it, 8) }
        GLES20.glLineWidth(watmcdLineWidth)
        listOf(mpdBuffers, mcdBuffers, watchBuffers, watchTornadoBuffers, swoBuffers).forEach { drawPolygons(it, 8) }
    }


    //https://stackoverflow.com/questions/10119479/calculating-lat-and-long-from-bearing-and-distance
    fun getNewConusPoint(latitude: Double, longitude: Double, distance: Double) {
        val brngRad = toRadians(135.0) //to SW
        val latRad = toRadians(latitude)
        val lonRad = toRadians(longitude)
        //val earthRadiusInMetres = 6371 //was 6371000
        val distFrac = distance / 6371

        val latitudeResult = asin(sin(latRad) * cos(distFrac) + cos(latRad) * sin(distFrac) * cos(brngRad))
        val a = atan2(sin(brngRad) * sin(distFrac) * cos(latRad), cos(distFrac) - sin(latRad) * sin(latitudeResult))
        val longitudeResult = (lonRad + a + 3 * PI) % (2 * PI) - PI

        newbottom = toDegrees(latitudeResult)
        newleft = toDegrees(longitudeResult)

        UtilityLog.d("wx", "newbottom: " + toDegrees(latitudeResult) + ", newleft: " + toDegrees(longitudeResult))
    }


    private fun drawConusRadarTest(buffers: ObjectOglBuffers) {
        if (buffers.isInitialized) {
            buffers.setToPositionZero()

            var vertexBuffer: FloatBuffer
            var drawListBuffer: ShortBuffer
            var uvBuffer: FloatBuffer


            //use conus shader
            GLES20.glUseProgram(OpenGLShader.sp_conus)



            val conusbitmap: Bitmap? = OpenGLShader.LoadBitmap(context.filesDir.toString() + "/conus.gif")
            val ridx = Utility.readPref(context, "RID_" + rid + "_X", "0.0f").toFloat()
            val ridy = Utility.readPref(context, "RID_" + rid + "_Y", "0.0f").toFloat() / -1.0
            UtilityLog.d("wx", rid + " rid x: " + ridx + " y: " + ridy)

            UtilityLog.d("wx", "gfw1: " + UtilityConusRadar.gfw1)
            UtilityLog.d("wx", "gfw2: " + UtilityConusRadar.gfw2)
            UtilityLog.d("wx", "gfw3: " + UtilityConusRadar.gfw3)
            UtilityLog.d("wx", "gfw4: " + UtilityConusRadar.gfw4)
            UtilityLog.d("wx", "gfw5: " + UtilityConusRadar.gfw5)
            UtilityLog.d("wx", "gfw6: " + UtilityConusRadar.gfw6)
            degreesPerPixellon = UtilityConusRadar.gfw1.toDouble()
            degreesPerPixellat = UtilityConusRadar.gfw4.toDouble()
            west = UtilityConusRadar.gfw5.toDouble()
            north = UtilityConusRadar.gfw6.toDouble()
            south = north + conusbitmap!!.height.toDouble() * degreesPerPixellat
            east = west + conusbitmap!!.width.toDouble() * degreesPerPixellon


            UtilityLog.d("wx", "north: " + north)
            UtilityLog.d("wx", "south: " + south)
            UtilityLog.d("wx", "west: " + west)
            UtilityLog.d("wx", "east: " + east)

            //from aweather
            //https://github.com/Andy753421/AWeather
            var asouth = north - UtilityConusRadar.gfw1.toDouble() * conusbitmap.height.toDouble()
            var aeast = west + UtilityConusRadar.gfw1.toDouble() * conusbitmap.width.toDouble()
            var midofwest = west + UtilityConusRadar.gfw1.toDouble() * conusbitmap.width.toDouble() / 2
            var midofsouth = north - UtilityConusRadar.gfw1.toDouble() * conusbitmap.height.toDouble() / 2

            UtilityLog.d("wx", "asouth: " + asouth)
            UtilityLog.d("wx", "aeast: " + aeast)
            UtilityLog.d("wx", "midofwest: " + midofwest)
            UtilityLog.d("wx", "midofsouth: " + midofsouth)


            //val riddist = LatLon.distance(LatLon(ridx.toDouble(), ridy.toDouble()), LatLon(south, west), DistanceUnit.MILE)
            //UtilityLog.d("wx", "riddist: " + riddist)
            //getNewConusPoint(south, west, riddist)





            //triangle
            val base = RectF(-5000f, 3400f, 3400f, -5000f)
            //val scale = 1f

            val left = base.left
            val right = base.right
            val bottom = base.bottom
            val top = base.top

            val vertices = floatArrayOf(
                    left, top, 0.0f,
                    left, bottom, 0.0f,
                    right, bottom, 0.0f,
                    right, top, 0.0f)


            val indices = shortArrayOf(0, 1, 2, 0, 2, 3) // The order of vertexrendering.

            // The vertex buffer.
            val vbb = ByteBuffer.allocateDirect(vertices.size * 4)
            vbb.order(ByteOrder.nativeOrder())
            vertexBuffer = vbb.asFloatBuffer()
            vertexBuffer.put(vertices)
            vertexBuffer.position(0)

            // initialize byte buffer for the draw list
            val dlb = ByteBuffer.allocateDirect(indices.size * 2)
            dlb.order(ByteOrder.nativeOrder())
            drawListBuffer = dlb.asShortBuffer()
            drawListBuffer.put(indices)
            drawListBuffer.position(0)


            //texture
            val uvs = floatArrayOf(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f)

            // The texture buffer
            val tbb = ByteBuffer.allocateDirect(uvs.size * 4)
            tbb.order(ByteOrder.nativeOrder())
            uvBuffer = tbb.asFloatBuffer()
            uvBuffer.put(uvs)
            uvBuffer.position(0)
            OpenGLShader.LoadImage(context.filesDir.toString() + "/conus.gif")


            val mPositionHandle = GLES20.glGetAttribLocation(OpenGLShader.sp_conus, "vPosition")
            GLES20.glEnableVertexAttribArray(mPositionHandle)
            GLES20.glVertexAttribPointer(mPositionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer)
            val mTexCoordLoc = GLES20.glGetAttribLocation(OpenGLShader.sp_conus, "a_texCoords")
            GLES20.glEnableVertexAttribArray(mTexCoordLoc)
            GLES20.glVertexAttribPointer(mTexCoordLoc, 2, GLES20.GL_FLOAT, false, 0, uvBuffer)
            val mtrxhandle = GLES20.glGetUniformLocation(OpenGLShader.sp_conus, "uMVPMatrix")
            GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, mtrxProjectionAndView, 0)
            val conusTexture = GLES20.glGetUniformLocation(OpenGLShader.sp_conus, "u_texture")
            GLES20.glUniform1i(conusTexture, 0)
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, indices.size, GLES20.GL_UNSIGNED_SHORT, drawListBuffer)

            // Disable vertex array
            GLES20.glDisableVertexAttribArray(mPositionHandle)
            GLES20.glDisableVertexAttribArray(mTexCoordLoc)
            //back to regular shader
            GLES20.glUseProgram(OpenGLShader.sp_SolidColor)

        }

    }





    private fun drawConusRadar(buffers: ObjectOglBuffers) {
        if (buffers.isInitialized) {
            buffers.setToPositionZero()
            GLES20.glUseProgram(OpenGLShader.sp_loadconus)
            mPositionHandle = GLES20.glGetAttribLocation(OpenGLShader.sp_loadconus, "vPosition")


            UtilityLog.d("wx", "oneDegreeScaleFactorGlobal: " + oneDegreeScaleFactorGlobal)
            UtilityLog.d("wx", "conus gpsx: " + gpsX + " gpsy: " + gpsY)
            UtilityLog.d("wx", "conus x: " + x + " y: " + y)
            UtilityLog.d("wx", "conus pn xcenter: " + pn.xCenter + " ycenter: " + pn.yCenter)
            UtilityLog.d("wx", "conus pn scale: " + pn.scale + " pn scalefloat: " + pn.scaleFloat)
            UtilityLog.d("wx", "conus position x: " + positionXGlobal + " y: " + positionYGlobal)

            GLES20.glUniformMatrix4fv(GLES20.glGetUniformLocation(OpenGLShader.sp_loadconus, "uMVPMatrix"), 1, false, mtrxProjectionAndView, 0)
            iTexture = GLES20.glGetUniformLocation(OpenGLShader.sp_loadconus, "u_texture")
            //conusradarId = OpenGLShader.LoadTexture(Environment.getExternalStorageDirectory().getAbsolutePath() + "/wX/test.gif")
            conusradarId = OpenGLShader.LoadTexture(context.filesDir.toString() + "/conus.gif")
            val bitmap: Bitmap? = OpenGLShader.LoadBitmap(context.filesDir.toString() + "/conus.gif")
            val conuswidth = bitmap!!.width.toFloat()
            val conusheight = bitmap!!.height.toFloat()
            val imagesize: Int = 8


            /*
            val data = floatArrayOf(

                    0f, 0f,
                    0f, 0f, //Texture coordinate for V1

                    0f, conusheight * imagesize, //V2
                    0f, 1f,

                    conuswidth * imagesize, 0f, //V3
                    1f, 0f,

                    conuswidth* imagesize, conusheight * imagesize, //V4
                    1f, 1f)
*/


            val x: Float = 0.0f
            val y: Float = 0.0f
            val data = floatArrayOf(
                    x, y, //V1
                    0f, 0f, //Texture coordinate for V1
                    x, conusheight, //V2
                    0f, 1f, conuswidth, y, //V3
                    1f, 0f, conuswidth, conusheight, //V4
                    1f, 1f)


            val aTexPos = GLES20.glGetAttribLocation(OpenGLShader.sp_loadconus, "a_texCoords")

            // Again, a FloatBuffer will be used to pass the values
            val b = ByteBuffer.allocateDirect(data.size * 4).order(ByteOrder.nativeOrder()).asFloatBuffer()
            b.put(data)
            //b.put(buffers.floatBuffer.slice().asFloatBuffer())
            // Position of our image
            b.position(0)
            GLES20.glVertexAttribPointer(mPositionHandle, 2, GLES20.GL_FLOAT, false, 4 * 4, b)
            GLES20.glEnableVertexAttribArray(mPositionHandle)


            // Positions of the texture
            b.position(2)
            GLES20.glVertexAttribPointer(aTexPos, 2, GLES20.GL_FLOAT, false, 4 * 4, b)
            GLES20.glEnableVertexAttribArray(aTexPos)

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, conusradarId)
            GLES20.glUniform1i(iTexture, 0)
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4)
            GLES20.glUseProgram(OpenGLShader.sp_SolidColor)
        }
    }






    private fun drawConusRadarOrigBlog(buffers: ObjectOglBuffers) {
        if (buffers.isInitialized) {
            buffers.setToPositionZero()

    var vertexBuffer: FloatBuffer
    var drawListBuffer: ShortBuffer
    var uvBuffer: FloatBuffer


    //use conus shader
    GLES20.glUseProgram(OpenGLShader.sp_conus)

    //triangle
    val base = RectF(-5000f, 3400f, 3400f, -5000f)
    val scale = 1f

    val left = base.left * scale
    val right = base.right * scale
    val bottom = base.bottom * scale
    val top = base.top * scale

    val vertices = floatArrayOf(
            left, top, 0.0f,
            left, bottom, 0.0f,
            right, bottom, 0.0f,
            right, top, 0.0f)


    val indices = shortArrayOf(0, 1, 2, 0, 2, 3) // The order of vertexrendering.

    // The vertex buffer.
    val vbb = ByteBuffer.allocateDirect(vertices.size * 4)
    vbb.order(ByteOrder.nativeOrder())
    vertexBuffer = vbb.asFloatBuffer()
    vertexBuffer.put(vertices)
    vertexBuffer.position(0)

    // initialize byte buffer for the draw list
    val dlb = ByteBuffer.allocateDirect(indices.size * 2)
    dlb.order(ByteOrder.nativeOrder())
    drawListBuffer = dlb.asShortBuffer()
    drawListBuffer.put(indices)
    drawListBuffer.position(0)


    //texture
    val uvs = floatArrayOf(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f)

    // The texture buffer
    val tbb = ByteBuffer.allocateDirect(uvs.size * 4)
    tbb.order(ByteOrder.nativeOrder())
    uvBuffer = tbb.asFloatBuffer()
    uvBuffer.put(uvs)
    uvBuffer.position(0)
    OpenGLShader.LoadImage(context.filesDir.toString() + "/conus.gif")


    val mPositionHandle = GLES20.glGetAttribLocation(OpenGLShader.sp_conus, "vPosition")
    GLES20.glEnableVertexAttribArray(mPositionHandle)
    GLES20.glVertexAttribPointer(mPositionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer)
    val mTexCoordLoc = GLES20.glGetAttribLocation(OpenGLShader.sp_conus, "a_texCoords")
    GLES20.glEnableVertexAttribArray(mTexCoordLoc)
    GLES20.glVertexAttribPointer(mTexCoordLoc, 2, GLES20.GL_FLOAT, false, 0, uvBuffer)
    val mtrxhandle = GLES20.glGetUniformLocation(OpenGLShader.sp_conus, "uMVPMatrix")
    GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, mtrxProjectionAndView, 0)
    val conusTexture = GLES20.glGetUniformLocation(OpenGLShader.sp_conus, "u_texture")
    GLES20.glUniform1i(conusTexture, 0)
    GLES20.glEnable(GLES20.GL_BLEND);
    GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
    GLES20.glDrawElements(GLES20.GL_TRIANGLES, indices.size, GLES20.GL_UNSIGNED_SHORT, drawListBuffer)

    // Disable vertex array
    GLES20.glDisableVertexAttribArray(mPositionHandle)
    GLES20.glDisableVertexAttribArray(mTexCoordLoc)
    //back to regular shader
    GLES20.glUseProgram(OpenGLShader.sp_SolidColor)

}

}




        private fun drawConusRadarpt(buffers: ObjectOglBuffers) {
        if (buffers.isInitialized) {
            buffers.setToPositionZero()
            GLES20.glUseProgram(OpenGLShader.sp_loadimage)
            mPositionHandle = GLES20.glGetAttribLocation(OpenGLShader.sp_loadimage, "vPosition")
            GLES20.glUniformMatrix4fv(GLES20.glGetUniformLocation(OpenGLShader.sp_loadimage, "uMVPMatrix"), 1, false, mtrxProjectionAndView, 0)
            mSizeHandle = GLES20.glGetUniformLocation(OpenGLShader.sp_loadimage, "imagesize")
            var conusbitmap: Bitmap? = OpenGLShader.LoadBitmap(context.filesDir.toString() + "/conus.gif")

            UtilityLog.d("wx", "oneDegreeScaleFactorGlobal: "+oneDegreeScaleFactorGlobal)
            UtilityLog.d("wx", "conus gpsx: "+gpsX+" gpsy: "+gpsY)
            UtilityLog.d("wx", "conus x: "+x+" y: "+y)
            UtilityLog.d("wx", "conus pn xcenter: "+pn.xCenter+" ycenter: "+pn.yCenter)
            UtilityLog.d("wx", "conus pn scale: "+pn.scale+" pn scalefloat: "+pn.scaleFloat)
            UtilityLog.d("wx", "conus position x: "+ positionXGlobal+" y: "+ positionYGlobal)


            var conuswidth = conusbitmap!!.width
            var conusheight = conusbitmap!!.height
            var conusRatio = conuswidth.toFloat() / conusheight
            UtilityLog.d("wx", "conus ratio: "+ conusRatio)



            var setsize = 3400f * 3
            UtilityLog.d("wx", "mSizeHandle: "+setsize)
            GLES20.glUniform1f(mSizeHandle, setsize)

            iTexture = GLES20.glGetUniformLocation(OpenGLShader.sp_loadimage, "u_texture")

            //conusradarId = OpenGLShader.LoadTexture(Environment.getExternalStorageDirectory().getAbsolutePath() + "/wX/testconus.gif")
            conusradarId = OpenGLShader.LoadTexture(context.filesDir.toString() + "/conus.gif")

            GLES20.glVertexAttribPointer(mPositionHandle, 2, GLES20.GL_FLOAT, false, 0, buffers.floatBuffer.slice().asFloatBuffer())
            GLES20.glEnableVertexAttribArray(mPositionHandle)
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, conusradarId)
            GLES20.glUniform1i(iTexture, 0)
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
            //GLES20.glDrawElements(GLES20.GL_POINTS, 1, GLES20.GL_UNSIGNED_SHORT, buffers.indexBuffer.slice().asShortBuffer())
            GLES20.glDrawElements(GLES20.GL_POINTS, buffers.floatBuffer.capacity() / 8, GLES20.GL_UNSIGNED_SHORT, buffers.indexBuffer.slice().asShortBuffer())
            GLES20.glUseProgram(OpenGLShader.sp_SolidColor)
        }
    }

    private fun drawTriangles(buffers: ObjectOglBuffers) {
        if (buffers.isInitialized) {
            buffers.setToPositionZero()
            GLES20.glVertexAttribPointer(mPositionHandle, 2, GLES20.GL_FLOAT, false, 0, buffers.floatBuffer.slice().asFloatBuffer())
            GLES20.glVertexAttribPointer(colorHandle, 3, GLES20.GL_UNSIGNED_BYTE, true, 0, buffers.colorBuffer.slice().asFloatBuffer())
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, buffers.floatBuffer.capacity() / 8, GLES20.GL_UNSIGNED_SHORT, buffers.indexBuffer.slice().asShortBuffer())
        }
    }

    private fun drawPolygons(buffers: ObjectOglBuffers, countDivisor: Int) {
        if (buffers.isInitialized) {
            // FIXME is chunkcount ever above one? "it" is never reference in the loop
            (0 until buffers.chunkCount).forEach { _ ->
                lineIndexBuffer.position(0)
                buffers.setToPositionZero()
                GLES20.glVertexAttribPointer(
                    mPositionHandle,
                    2,
                    GLES20.GL_FLOAT,
                    false,
                    0,
                    buffers.floatBuffer.slice().asFloatBuffer()
                )
                GLES20.glVertexAttribPointer(
                    colorHandle,
                    3,
                    GLES20.GL_UNSIGNED_BYTE,
                    true,
                    0,
                    buffers.colorBuffer
                )
                GLES20.glDrawElements(
                    GLES20.GL_LINES,
                    buffers.floatBuffer.capacity() / countDivisor,
                    GLES20.GL_UNSIGNED_SHORT,
                    lineIndexBuffer.slice().asShortBuffer()
                )
            }
        }
    }

    private fun drawElement(buffers: ObjectOglBuffers) {
        if (buffers.isInitialized) {
            (0 until buffers.chunkCount).forEach {
                lineCnt = if (it < buffers.chunkCount - 1) {
                    breakSizeLine * 2
                } else {
                    2 * (buffers.count / 4 - it * breakSizeLine)
                }
                try {
                    buffers.floatBuffer.position(it * 480000)
                    buffers.colorBuffer.position(0)
                    lineIndexBuffer.position(0)
                    GLES20.glVertexAttribPointer(
                        mPositionHandle,
                        2,
                        GLES20.GL_FLOAT,
                        false,
                        0,
                        buffers.floatBuffer.slice().asFloatBuffer()
                    )
                    GLES20.glVertexAttribPointer(
                        colorHandle,
                        3,
                        GLES20.GL_UNSIGNED_BYTE,
                        true,
                        0,
                        buffers.colorBuffer.slice()
                    )
                    GLES20.glDrawElements(
                        GLES20.GL_LINES,
                        lineCnt,
                        GLES20.GL_UNSIGNED_SHORT,
                        lineIndexBuffer.slice().asShortBuffer()
                    )
                } catch (e: Exception) {

                }
            }
        }
    }

    override fun onSurfaceChanged(gl: GL10, width: Int, height: Int) {
        mSurfaceRatio = width.toFloat() / height
        (0..15).forEach {
            mtrxProjection[it] = 0.0f
            mtrxView[it] = 0.0f
            mtrxProjectionAndView[it] = 0.0f
        }

        //for conus
        (0..15).forEach {
            conusProjection[it] = 0.0f
            conusView[it] = 0.0f
            conusProjectionAndView[it] = 0.0f
        }


        Matrix.orthoM(mtrxProjection, 0, (-1 * ortInt).toFloat(), ortInt.toFloat(), -1f * ortInt.toFloat() * (1 / mSurfaceRatio), ortInt * (1 / mSurfaceRatio), 1f, -1f)
        Matrix.setLookAtM(mtrxView, 0, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 1.0f, 0.0f)
        Matrix.multiplyMM(mtrxProjectionAndView, 0, mtrxProjection, 0, mtrxView, 0)
        Matrix.multiplyMM(mtrxProjectionAndViewOrig, 0, mtrxProjection, 0, mtrxView, 0)
        Matrix.translateM(mtrxProjectionAndView, 0, x, y, 0f)
        Matrix.scaleM(mtrxProjectionAndView, 0, zoom, zoom, 1f)


        //for conus
        Matrix.orthoM(conusProjection,0, (-1 * ortInt).toFloat(), ortInt.toFloat(), -1f * ortInt.toFloat() * (1 / mSurfaceRatio), ortInt * (1 / mSurfaceRatio), 1f, -1f)
        Matrix.setLookAtM(conusView, 0, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 1.0f, 0.0f)
        Matrix.multiplyMM(conusProjectionAndView, 0, conusProjection, 0, conusView, 0)
        Matrix.translateM(conusProjectionAndView, 0, x, y, 0f)
        Matrix.scaleM(conusProjectionAndView, 0, zoom, zoom, 1f)
        UtilityLog.d("wx", "ProjectionAndView x: "+x+" y: "+y)

    }

    private fun scaleLength(currentLength: Float): Float {
        return if (zoom > 1.01f) {
            currentLength / zoom * 2
        } else {
            currentLength
        }
    }

    fun constructStateLines() {
        constructGenericGeographic(stateLineBuffers)
    }

    fun constructHWLines() {
        constructGenericGeographic(hwBuffers)
    }

    fun deconstructHWLines() {
        deconstructGenericGeographic(hwBuffers)
    }

    fun constructHWEXTLines() {
        constructGenericGeographic(hwExtBuffers)
    }

    fun deconstructHWEXTLines() {
        deconstructGenericGeographic(hwExtBuffers)
    }

    fun constructLakes() {
        constructGenericGeographic(lakeBuffers)
    }

    fun deconstructLakes() {
        deconstructGenericGeographic(lakeBuffers)
    }

    fun constructCounty() {
        constructGenericGeographic(countyLineBuffers)
    }

    fun deconstructCounty() {
        deconstructGenericGeographic(countyLineBuffers)
    }

    // FIXME this check for 4326 will need to be done in other locations as well but for now just testing to see
    // if the rectangular projection is realized.
    private fun constructGenericGeographic(buffers: ObjectOglBuffers) {
        if (!buffers.isInitialized) {
            buffers.count = buffers.geotype.count
            buffers.breakSize = 30000
            buffers.initialize(
                4 * buffers.count,
                0,
                3 * buffers.breakSize * 2,
                buffers.geotype.color
            )
            if (MyApplication.radarUseJni) {
                JNI.colorGen(buffers.colorBuffer, buffers.breakSize * 2, buffers.colorArray)
            } else {
                UtilityWXOGLPerf.colorGen(
                    buffers.colorBuffer,
                    buffers.breakSize * 2,
                    buffers.colorArray
                )
            }
            buffers.isInitialized = true
        }
        if (!MyApplication.radarUseJni) {
            if (useMercatorProjection) {
                UtilityWXOGLPerf.genMercator(
                    buffers.geotype.relativeBuffer,
                    buffers.floatBuffer,
                    pn,
                    buffers.count
                )
            } else {
                UtilityWXOGLPerf.generate4326Projection(
                    buffers.geotype.relativeBuffer,
                    buffers.floatBuffer,
                    pn,
                    buffers.count
                )
            }
        } else {
            if (useMercatorProjection) {
                JNI.genMercato(
                    buffers.geotype.relativeBuffer,
                    buffers.floatBuffer,
                    pn.xFloat,
                    pn.yFloat,
                    pn.xCenter.toFloat(),
                    pn.yCenter.toFloat(),
                    pn.oneDegreeScaleFactorFloat,
                    buffers.count
                )
            } else {
                // FIXME - will want native code version for 4326
                UtilityWXOGLPerf.generate4326Projection(
                    buffers.geotype.relativeBuffer,
                    buffers.floatBuffer,
                    pn,
                    buffers.count
                )
            }
        }
        buffers.setToPositionZero()
    }

    private fun deconstructGenericGeographic(buffers: ObjectOglBuffers) {
        buffers.isInitialized = false
    }

    private fun constructGenericLinesShort(buffers: ObjectOglBuffers, f: List<Double>) {
        val remainder: Int
        buffers.initialize(4 * 4 * f.size, 0, 3 * 4 * f.size, buffers.type.color)
        try {
            if (MyApplication.radarUseJni) {
                JNI.colorGen(buffers.colorBuffer, 4 * f.size, buffers.colorArray)
            } else {
                UtilityWXOGLPerf.colorGen(buffers.colorBuffer, 4 * f.size, buffers.colorArray)
            }
        } catch (e: java.lang.Exception) {
            UtilityLog.HandleException(e)
        }
        buffers.breakSize = 15000
        buffers.chunkCount = 1
        val totalBinsSti = f.size / 4
        if (totalBinsSti < buffers.breakSize) {
            buffers.breakSize = totalBinsSti
            remainder = buffers.breakSize
        } else {
            buffers.chunkCount = totalBinsSti / buffers.breakSize
            remainder = totalBinsSti - buffers.breakSize * buffers.chunkCount
            buffers.chunkCount += 1
        }
        var vList = 0
        (0 until buffers.chunkCount).forEach {
            if (it == buffers.chunkCount - 1) {
                buffers.breakSize = remainder
            }
            (0 until buffers.breakSize).forEach { _ ->
                buffers.putFloat(f[vList].toFloat())
                buffers.putFloat(f[vList + 1].toFloat() * -1)
                buffers.putFloat(f[vList + 2].toFloat())
                buffers.putFloat(f[vList + 3].toFloat() * -1)
                vList += 4
            }
        }
        buffers.isInitialized = true
    }

    fun constructSTILines() {
        val fSti = WXGLNexradLevel3StormInfo.decodeAndPlot(context, idxStr, rid, provider)
        constructGenericLinesShort(stiBuffers, fSti)
    }

    fun deconstructSTILines() {
        deconstructGenericLines(stiBuffers)
    }

    fun constructWATMCDLines() {
        constructGenericLines(mcdBuffers)
        constructGenericLines(watchBuffers)
        constructGenericLines(watchTornadoBuffers)
    }

    fun deconstructWATMCDLines() {
        deconstructGenericLines(mcdBuffers)
        deconstructGenericLines(watchBuffers)
        deconstructGenericLines(watchTornadoBuffers)
    }

    fun constructWarningLines() {
        constructGenericLines(warningTstBuffers)
        constructGenericLines(warningTorBuffers)
        constructGenericLines(warningFfwBuffers)
    }

    fun deconstructWarningLines() {
        deconstructGenericLines(warningTstBuffers)
        deconstructGenericLines(warningTorBuffers)
        deconstructGenericLines(warningFfwBuffers)
    }

    fun constructLocationDot(locXCurrent: String, locYCurrentF: String, archiveMode: Boolean) {
        var locYCurrent = locYCurrentF
        var locmarkerAl = mutableListOf<Double>()
        locdotBuffers.lenInit = MyApplication.radarLocdotSize.toFloat()
        locYCurrent = locYCurrent.replace("-", "")
        val x = locXCurrent.toDoubleOrNull() ?: 0.0
        val y = locYCurrent.toDoubleOrNull() ?: 0.0
        if (PolygonType.LOCDOT.pref) {
            locmarkerAl = UtilityLocation.latLonAsDouble
        }
        if (MyApplication.locdotFollowsGps || archiveMode) {
            locmarkerAl.add(x)
            locmarkerAl.add(y)
            gpsX = x
            gpsY = y
        }
        locdotBuffers.xList = DoubleArray(locmarkerAl.size)
        locdotBuffers.yList = DoubleArray(locmarkerAl.size)
        var xx = 0
        var yy = 0
        locmarkerAl.indices.forEach {
            if (it and 1 == 0) {
                locdotBuffers.xList[xx] = locmarkerAl[it]
                xx += 1
            } else {
                locdotBuffers.yList[yy] = locmarkerAl[it]
                yy += 1
            }
        }
        locdotBuffers.triangleCount = 12
        constructTriangles(locdotBuffers)
        locCircleBuffers.triangleCount = 36
        locCircleBuffers.initialize(
            32 * locCircleBuffers.triangleCount,
            8 * locCircleBuffers.triangleCount,
            6 * locCircleBuffers.triangleCount,
            MyApplication.radarColorLocdot
        )
        if (MyApplication.radarUseJni) {
            JNI.colorGen(
                locCircleBuffers.colorBuffer,
                2 * locCircleBuffers.triangleCount,
                locCircleBuffers.colorArray
            )
        } else {
            UtilityWXOGLPerf.colorGen(
                locCircleBuffers.colorBuffer,
                2 * locCircleBuffers.triangleCount,
                locCircleBuffers.colorArray
            )
        }
        if (MyApplication.locdotFollowsGps) {
            locCircleBuffers.lenInit = locdotBuffers.lenInit
            UtilityWXOGLPerf.genCircleLocdot(locCircleBuffers, pn, gpsX, gpsY)
        }
        locdotBuffers.isInitialized = true
        locCircleBuffers.isInitialized = true

    }

    fun deconstructLocationDot() {
        locdotBuffers.isInitialized = false
    }


    /*
    *
    *
    <LatLonBox>
    <north>50.406626367301044</north>
    <south>21.652538062803</south>
    <east>-66.517937876818</east>
    <west>-127.620375523875420</west>
    </LatLonBox>

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val usa = LatLng(40.0, -100.0)
        //mMap.addMarker(MarkerOptions().position(usa).title("center of usa"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(usa))


        val newarkBounds = LatLngBounds(
                LatLng(21.652538062803, -127.620375523875420), // South west corner
                LatLng(50.406626367301044, -66.517937876818) // North east corner
        )
*/


//FIXME WTF?!?!?!?!?
    fun constructConusRadar() {
        conusRadarBuffers.lenInit = 0f

    /*
        conusRadarBuffers.triangleCount = 1 //was 36
        conusRadarBuffers.initialize(32 * conusRadarBuffers.triangleCount,
                8 * conusRadarBuffers.triangleCount,
                6 * conusRadarBuffers.triangleCount,
                0)

     */


    /*
        degreesPerPixellon = UtilityConusRadar.gfw1.toDouble()
        degreesPerPixellat = UtilityConusRadar.gfw4.toDouble()
        west = UtilityConusRadar.gfw5.toDouble()
        north = UtilityConusRadar.gfw6.toDouble()
        south = north + 1600 * degreesPerPixellat
        east = west + 3400 * degreesPerPixellon
   */
        //UtilityLog.d("wx", "north: "+north)
        //UtilityLog.d("wx", "south: "+south)
        //UtilityLog.d("wx", "west: "+west)
        //UtilityLog.d("wx", "east: "+east)



    //editor.putString("RID_latest_X", "36.105") // nws conus
    //editor.putString("RID_latest_Y", "97.141")


    //UtilityWXOGLPerf.genMarker(conusRadarBuffers, pn, 36.105, 97.141)

        //UtilityWXOGLPerf.genConus(conusRadarBuffers, pnconus, 50.0, 127.0)
        //FIXME need to find true center of the map....
        //UtilityWXOGLPerf.genCircleLocdot(conusRadarBuffers, pn, 40.0, 100.0)
        //UtilityWXOGLPerf.genCircleLocdot(conusRadarBuffers, pn, 40.0, 100.0)
        //UtilityWXOGLPerf.genCircleLocdot(conusRadarBuffers, pn, pn.xCenter, pn.yCenter)
        //UtilityWXOGLPerf.genMercato(MyApplication.stateRelativeBuffer, conusRadarBuffers.floatBuffer, pn, conusRadarBuffers.count)
        //UtilityWXOGLPerf.generate4326Projection(MyApplication.stateRelativeBuffer, conusRadarBuffers.floatBuffer, pn, conusRadarBuffers.count)

        conusRadarBuffers.isInitialized = true
    }

    fun deconstructConusRadar() {
        conusRadarBuffers.isInitialized = false
    }



    fun constructSpotters() {
        spotterBuffers.isInitialized = false
        spotterBuffers.lenInit = MyApplication.radarSpotterSize.toFloat()
        spotterBuffers.triangleCount = 6
        UtilitySpotter.spotterData
        spotterBuffers.xList = UtilitySpotter.x
        spotterBuffers.yList = UtilitySpotter.y
        constructTriangles(spotterBuffers)
    }

    fun deconstructSpotters() {
        spotterBuffers.isInitialized = false
    }

    fun constructHI() {
        hiBuffers.lenInit = MyApplication.radarHiSize.toFloat()
        val stormList = WXGLNexradLevel3HailIndex.decodeAndPlot(context, rid, idxStr)
        hiBuffers.setXYList(stormList)
        constructTriangles(hiBuffers)
    }

    private fun constructTriangles(buffers: ObjectOglBuffers) {
        buffers.count = buffers.xList.size
        when (buffers.type) {
            PolygonType.LOCDOT, PolygonType.SPOTTER -> buffers.initialize(
                24 * buffers.count * buffers.triangleCount,
                12 * buffers.count * buffers.triangleCount,
                9 * buffers.count * buffers.triangleCount,
                buffers.type.color
            )
            else -> buffers.initialize(
                4 * 6 * buffers.count,
                4 * 3 * buffers.count,
                9 * buffers.count,
                buffers.type.color
            )
        }
        buffers.lenInit = scaleLength(buffers.lenInit)
        buffers.draw(pn)
        buffers.isInitialized = true
    }

    fun deconstructHI() {
        hiBuffers.isInitialized = false
    }

    fun constructTVS() {
        tvsBuffers.lenInit = MyApplication.radarTvsSize.toFloat()
        val stormList = WXGLNexradLevel3TVS.decodeAndPlot(context, rid, idxStr)
        tvsBuffers.setXYList(stormList)
        constructTriangles(tvsBuffers)
    }

    fun deconstructTVS() {
        tvsBuffers.isInitialized = false
    }

    fun constructMPDLines() {
        constructGenericLines(mpdBuffers)
    }

    fun deconstructMPDLines() {
        deconstructGenericLines(mpdBuffers)
    }

    private fun constructGenericLines(buffers: ObjectOglBuffers) {
        var fList = listOf<Double>()
        when (buffers.type) {
            PolygonType.MCD, PolygonType.MPD, PolygonType.WATCH, PolygonType.WATCH_TORNADO -> fList =
                    UtilityWatch.addWat(context, provider, rid, buffers.type).toList()
            PolygonType.TST, PolygonType.TOR, PolygonType.FFW -> fList =
                    WXGLPolygonWarnings.addWarnings(context, provider, rid, buffers.type).toList()
            PolygonType.STI -> fList =
                    WXGLNexradLevel3StormInfo.decodeAndPlot(context, idxStr, rid, provider).toList()
            else -> {
            }
        }
        buffers.breakSize = 15000
        buffers.chunkCount = 1
        val totalBinsGeneric = fList.size / 4
        var remainder = 0
        if (totalBinsGeneric < buffers.breakSize) {
            buffers.breakSize = totalBinsGeneric
            remainder = buffers.breakSize
        } else if (buffers.breakSize > 0) {
            buffers.chunkCount = totalBinsGeneric / buffers.breakSize
            remainder = totalBinsGeneric - buffers.breakSize * buffers.chunkCount
            buffers.chunkCount = buffers.chunkCount + 1
        }
        buffers.initialize(
            4 * 4 * totalBinsGeneric,
            0,
            3 * 4 * totalBinsGeneric,
            buffers.type.color
        )
        if (MyApplication.radarUseJni) {
            JNI.colorGen(buffers.colorBuffer, 4 * totalBinsGeneric, buffers.colorArray)
        } else {
            UtilityWXOGLPerf.colorGen(buffers.colorBuffer, 4 * totalBinsGeneric, buffers.colorArray)
        }
        var vList = 0
        (0 until buffers.chunkCount).forEach {
            if (it == buffers.chunkCount - 1) {
                buffers.breakSize = remainder
            }
            (0 until buffers.breakSize).forEach { _ ->
                if (fList.size > (vList + 3)) {
                    buffers.putFloat(fList[vList].toFloat())
                    buffers.putFloat(fList[vList + 1].toFloat() * -1.0f)
                    buffers.putFloat(fList[vList + 2].toFloat())
                    buffers.putFloat(fList[vList + 3].toFloat() * -1.0f)
                    vList += 4
                }
            }
        }
        buffers.isInitialized = true
    }

    private fun deconstructGenericLines(buffers: ObjectOglBuffers) {
        buffers.chunkCount = 0
        buffers.isInitialized = false
    }

    fun constructWBLines() {
        val fWb = WXGLNexradLevel3WindBarbs.decodeAndPlot(context, rid, provider, false)
        constructGenericLinesShort(wbBuffers, fWb)
        constructWBLinesGusts()
        constructWBCircle()
    }

    private fun constructWBLinesGusts() {
        val fWbGusts = WXGLNexradLevel3WindBarbs.decodeAndPlot(context, rid, provider, true)
        constructGenericLinesShort(wbGustsBuffers, fWbGusts)
    }

    fun deconstructWBLines() {
        wbBuffers.isInitialized = false
        deconstructWBLinesGusts()
        deconstructWBCircle()
    }

    private fun deconstructWBLinesGusts() {
        wbGustsBuffers.isInitialized = false
    }

    private fun constructWBCircle() {
        wbCircleBuffers.lenInit = MyApplication.radarAviationSize.toFloat()
        wbCircleBuffers.xList = UtilityMetar.x
        wbCircleBuffers.yList = UtilityMetar.y
        wbCircleBuffers.colorIntArray = UtilityMetar.obsArrAviationColor
        wbCircleBuffers.count = wbCircleBuffers.xList.size
        wbCircleBuffers.triangleCount = 6
        wbCircleBuffers.initialize(
            24 * wbCircleBuffers.count * wbCircleBuffers.triangleCount,
            12 * wbCircleBuffers.count * wbCircleBuffers.triangleCount,
            9 * wbCircleBuffers.count * wbCircleBuffers.triangleCount
        )
        wbCircleBuffers.lenInit = scaleLength(wbCircleBuffers.lenInit)
        wbCircleBuffers.draw(pn)
        wbCircleBuffers.isInitialized = true
    }

    private fun deconstructWBCircle() {
        wbCircleBuffers.isInitialized = false
    }

    fun constructSWOLines() {
        val hashSWO = UtilitySWOD1.HASH_SWO.toMap()
        colorSwo[0] = Color.MAGENTA
        colorSwo[1] = Color.RED
        colorSwo[2] = Color.rgb(255, 140, 0)
        colorSwo[3] = Color.YELLOW
        colorSwo[4] = Color.rgb(0, 100, 0)
        var tmpCoords: DoubleArray
        val fSize = (0..4).filter { hashSWO[it] != null }.sumBy { hashSWO[it]!!.size }
        swoBuffers.breakSize = 15000
        swoBuffers.chunkCount = 1
        val totalBinsSwo = fSize / 4
        swoBuffers.initialize(4 * 4 * totalBinsSwo, 0, 3 * 2 * totalBinsSwo)
        if (totalBinsSwo < swoBuffers.breakSize) {
            swoBuffers.breakSize = totalBinsSwo
        } else {
            swoBuffers.chunkCount = totalBinsSwo / swoBuffers.breakSize
            swoBuffers.chunkCount = swoBuffers.chunkCount + 1
        }
        swoBuffers.isInitialized = true
        (0..4).forEach {
            if (hashSWO[it] != null) {
                var j = 0
                while (j < hashSWO[it]!!.size) {
                    swoBuffers.putColor(Color.red(colorSwo[it]).toByte())
                    swoBuffers.putColor(Color.green(colorSwo[it]).toByte())
                    swoBuffers.putColor(Color.blue(colorSwo[it]).toByte())
                    swoBuffers.putColor(Color.red(colorSwo[it]).toByte())
                    swoBuffers.putColor(Color.green(colorSwo[it]).toByte())
                    swoBuffers.putColor(Color.blue(colorSwo[it]).toByte())
                    tmpCoords = UtilityCanvasProjection.computeMercatorNumbers(
                        hashSWO[it]!![j],
                        (hashSWO[it]!![j + 1] * -1.0f),
                        pn
                    )
                    swoBuffers.putFloat(tmpCoords[0].toFloat())
                    swoBuffers.putFloat(tmpCoords[1].toFloat() * -1.0f)
                    tmpCoords = UtilityCanvasProjection.computeMercatorNumbers(
                        hashSWO[it]!![j + 2],
                        (hashSWO[it]!![j + 3] * -1.0f),
                        pn
                    )
                    swoBuffers.putFloat(tmpCoords[0].toFloat())
                    swoBuffers.putFloat(tmpCoords[1].toFloat() * -1.0f)
                    j += 4
                }
            }
        }
    }

    fun deconstructSWOLines() {
        swoBuffers.isInitialized = false
    }

    fun setHiInit(hiInit: Boolean) {
        hiBuffers.isInitialized = hiInit
    }

    fun setTvsInit(tvsInit: Boolean) {
        tvsBuffers.isInitialized = tvsInit
    }

    val oneDegreeScaleFactor: Float
        get() = pn.oneDegreeScaleFactorFloat

    fun setChunkCountSti(chunkCountSti: Int) {
        this.stiBuffers.chunkCount = chunkCountSti
    }

    fun setChunkCount(chunkCount: Int) {
        this.chunkCount = chunkCount
    }

    fun setViewInitial(zoom: Float, x: Float, y: Float) {
        this.zoom = zoom
        this.x = x
        this.y = y
    }
}
