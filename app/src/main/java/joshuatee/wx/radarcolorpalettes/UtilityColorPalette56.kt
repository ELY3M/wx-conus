/*

    Copyright 2013, 2014, 2015, 2016, 2017, 2018, 2019  joshua.tee@gmail.com

    This file is part of wX.

    wX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    wX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with wX.  If not, see <http://www.gnu.org/licenses/>.

 */

package joshuatee.wx.radarcolorpalettes

import joshuatee.wx.MyApplication

internal object UtilityColorPalette56 {

    fun gen56() {
        val obj56 = MyApplication.colorMap[56]!!
        obj56.redValues.position(0)
        obj56.greenValues.position(0)
        obj56.blueValues.position(0)
        obj56.redValues.put(0.toByte())
        obj56.redValues.put(2.toByte())
        obj56.redValues.put(1.toByte())
        obj56.redValues.put(1.toByte())
        obj56.redValues.put(7.toByte())
        obj56.redValues.put(6.toByte())
        obj56.redValues.put(4.toByte())
        obj56.redValues.put(124.toByte())
        obj56.redValues.put(152.toByte())
        obj56.redValues.put(137.toByte())
        obj56.redValues.put(162.toByte())
        obj56.redValues.put(185.toByte())
        obj56.redValues.put(216.toByte())
        obj56.redValues.put(239.toByte())
        obj56.redValues.put(254.toByte())
        obj56.redValues.put(144.toByte())
        obj56.greenValues.put(0.toByte())
        obj56.greenValues.put(252.toByte())
        obj56.greenValues.put(228.toByte())
        obj56.greenValues.put(197.toByte())
        obj56.greenValues.put(172.toByte())
        obj56.greenValues.put(143.toByte())
        obj56.greenValues.put(114.toByte())
        obj56.greenValues.put(151.toByte())
        obj56.greenValues.put(119.toByte())
        obj56.greenValues.put(0.toByte())
        obj56.greenValues.put(0.toByte())
        obj56.greenValues.put(0.toByte())
        obj56.greenValues.put(0.toByte())
        obj56.greenValues.put(0.toByte())
        obj56.greenValues.put(0.toByte())
        obj56.blueValues.put(0.toByte())
        obj56.blueValues.put(2.toByte())
        obj56.blueValues.put(1.toByte())
        obj56.blueValues.put(1.toByte())
        obj56.blueValues.put(4.toByte())
        obj56.blueValues.put(3.toByte())
        obj56.blueValues.put(2.toByte())
        obj56.blueValues.put(123.toByte())
        obj56.blueValues.put(119.toByte())
        obj56.blueValues.put(0.toByte())
        obj56.blueValues.put(0.toByte())
        obj56.blueValues.put(0.toByte())
        obj56.blueValues.put(0.toByte())
        obj56.blueValues.put(0.toByte())
        obj56.blueValues.put(0.toByte())
        obj56.blueValues.put(160.toByte())
    }
}